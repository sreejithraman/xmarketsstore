package com.team.xmarketsstore;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class CategoryListFragment extends Fragment implements OnItemClickListener {

	View view;
	ListView listView;
	Context context;
	CategoryListClickHandler handler;
	ArrayAdapter<Category> adapter;
	
	String tag = this.getClass().getSimpleName();

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.i(tag, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity) 
	{
		Log.i(tag, "onAttach");
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(tag, "onCreate");
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
	{
		Log.i(tag, "onCreateView");
		view = inflater.inflate(R.layout.list_view_fragment, container, false);	
		
		return view;
	}

	public void updateAdapter()
	{
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onDestroy() {
		Log.i(tag, "onDestroy");
		super.onDestroy();
	}

	@Override
	public void onDestroyView() {
		Log.i(tag, "onDestroyView");
		super.onDestroyView();
	}

	@Override
	public void onDetach() {
		Log.i(tag, "onDetach");
		super.onDetach();
	}

	@Override
	public void onPause() {
		Log.i(tag, "onPause");
		super.onPause();
	}

	@Override
	public void onResume() {
		Log.i(tag, "onResume");
		super.onResume();
		
		ArrayList<Category> categoryList = Utilities.getCategoryList();

		context = getActivity();
		listView = (ListView) view.findViewById(R.id.listViewFragment);
		adapter = new ArrayAdapter<Category>(context, R.layout.text_row, R.id.textRow, categoryList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
		
		Button hideBackButton = new Button(context);
		hideBackButton = (Button)((Activity) context).findViewById(R.id.backCategoryButton);
		hideBackButton.setVisibility(view.INVISIBLE);
		
		try
		{
			handler = (CategoryListClickHandler) getActivity();
		}
		catch (ClassCastException e)
		{
			Log.i(tag, "Activity " + getActivity().getClass().getSimpleName() + " FAILED!");
		}
	}

	@Override
	public void onStart() {
		Log.i(tag, "onStart");
		super.onStart();
	}

	@Override
	public void onStop() {
		Log.i(tag, "onStop");
		super.onStop();
	}

	/*
	 *  public interface CategoryListClickHandler: used to send the users click position of the list so that the system can
	 *  	make the appropriate updates 
	 * 
	 * */

	public interface CategoryListClickHandler
	{
		public void onHandleCategoryClick(int position);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position , long arg3) 
	{	
		handler.onHandleCategoryClick(position);  
	}	

}











