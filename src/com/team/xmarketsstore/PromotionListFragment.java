package com.team.xmarketsstore;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

public class PromotionListFragment extends Fragment {

	View view;
	ListView listView;
	ArrayAdapter<String> adapter;
	Context context;
	
	FragmentManager fm;
	FragmentTransaction ft;
	
	String tag = this.getClass().getSimpleName();

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.i(tag, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity) {
		Log.i(tag, "onAttach");
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(tag, "onCreate");
		super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
	{
		Log.i(tag, "onCreateView");
		view = inflater.inflate(R.layout.list_view_fragment,container, false);
		return view;
	}
	
	@Override
	public void onDestroy() {
		Log.i(tag, "onDestroy");
		super.onDestroy();
	}

	@Override
	public void onDestroyView() {
		Log.i(tag, "onDestroyView");
		super.onDestroyView();
	}

	@Override
	public void onDetach() {
		Log.i(tag, "onDetach");
		super.onDetach();
	}

	@Override
	public void onPause() {
		Log.i(tag, "onPause");
		super.onPause();
	}

	@Override
	public void onResume() {
		Log.i(tag, "onResume");
		super.onResume();
		
		String[] temp = {"Hello", "This", "Is", "The", "Promotions", "Screen"};
		
		context = getActivity();	
		listView = (ListView) view.findViewById(R.id.listViewFragment);
//		listView.addView(view, R.layout.promotion_row);
		adapter = new ArrayAdapter<String>(context, R.layout.text_row, R.id.textRow, temp);
		listView.setAdapter(adapter);
	}

	@Override
	public void onStart() {
		Log.i(tag, "onStart");
		super.onStart();
	}

	@Override
	public void onStop() {
		Log.i(tag, "onStop");
		super.onStop();
	}
}
