package com.team.xmarketsstore;

import java.util.ArrayList;

import com.team.xmarketsstore.Promotion;
import com.team.xmarketsstore.Category;
import com.team.xmarketsstore.Item;

public class Store {

	private String name;
	private String address;
	private String email; 
	private String phone;
	private String userName;
	private String password;
	private int storeId;
	
	ArrayList<Promotion> promotions = new ArrayList<Promotion>();
	ArrayList<Category> categories = new ArrayList<Category>();
	ArrayList<Item> items = new ArrayList<Item>();
	
	public Store (String name, String address, String email, String phone, String userName, 
			String password, int storeId) 
	{
		this.name = name;
		this.address = address;
		this.email = email;
		this.phone = phone;
		this.userName = userName;
		this.password = password;
		this.storeId = storeId;	
	}

	
	/* GETTERS AND SETTERS */
	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getEmail() {
		return email;
	}

	public String getPhone() {
		return phone;
	}

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}

	public int getStoreId() {
		return storeId;
	}

	public ArrayList<Promotion> getPromotions() {
		return promotions;
	}

	public void setPromotions(ArrayList<Promotion> promotions) {
		this.promotions = promotions;
	}

	public ArrayList<Category> getCategories() {
		return categories;
	}

	public void setCategories(ArrayList<Category> categories) {
		this.categories = categories;
	}

	public ArrayList<Item> getItems() {
		return items;
	}

	public void setItems(ArrayList<Item> items) {
		this.items = items;
	}
	
	
	
	
	
	
	
	
	
/*
 * THE CODE BELOW DOES NOT SEEM TO FIT IN THIS CLASS BUT MAY BE USEFUL ELSEWARE
 * 
 * */
	
//	public Store GetStoreById(int id){
//		for(Store t : collection_stores) {
//			if(t.Id == id) { 
//				return t;	   
//			}
//		}
//		return null;	
//	}
//	
//	public ArrayList<Store> GetStoresByName(String name){
//		ArrayList<Store> output= new ArrayList<Store>();
//
//		for(Store c : collection_stores) {
//			if(c.Name.equalsIgnoreCase(name)) {
//				output.add(c);	   
//			}
//		}
//		return output;
//	}
//	
//	public ArrayList<Store> GetStoresByItem(Item d)
//	{
//		ArrayList<Store> output= new ArrayList<Store>();
//
//		for(Store c : collection_stores) 
//		{
//			for (Iterator<Item> iter = c.Inventory.iterator(); iter.hasNext(); ) {
//				Item f = iter.next();  
//				if(f.getId() == d.getId()) 
//				{
//					output.add(c);	   
//				}
//			}
//		}
//		return output;
//	}
//	
//	public ArrayList<Store> GetStoresByItemName(String name)
//	{
//		ArrayList<Store> output= new ArrayList<Store>();
//
//		for(Store c : collection_stores) 
//		{
//			for (Iterator<Item> iter = c.Inventory.iterator(); iter.hasNext(); ) {
//				Item f = iter.next();  
//				if(f.getName() == name) 
//				{
//					output.add(c);	   
//				}
//			}
//		}
//		return output;
//	}
//
//	public ArrayList<Store> GetStoresByCategory(Category d){
//		ArrayList<Store> output= new ArrayList<Store>();
//
//		for(Store c : collection_stores) 
//		{
//			for (Iterator<Item> iter = c.Inventory.iterator(); iter.hasNext(); ) {
//				Item f = iter.next();  
//				if(f.getCategoryId() == d.getId()) 
//				{
//					output.add(c);	   
//				}
//			}
//		}
//		return output;
//	}
//	
//	public ArrayList<Store> GetStoresByPromotion(Promotion p){
//		ArrayList<Store> output= new ArrayList<Store>();
//
//		for(Store c : collection_stores) 
//		{
//			for (Iterator<Item> iter = c.Inventory.iterator(); iter.hasNext(); ) {
//				Item f = iter.next();  
//				if(f.getPromotionId() == p.getId()) 
//				{
//					output.add(c);	   
//				}
//			}
//		}
//		return output;
//	}
}


