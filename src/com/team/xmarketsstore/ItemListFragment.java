package com.team.xmarketsstore;

import java.util.ArrayList;

import com.team.xmarketsstore.CategoryListFragment.CategoryListClickHandler;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

public class ItemListFragment extends Fragment implements OnItemClickListener {

	View view;
	ListView listView;
	Context context;
	ArrayAdapter<Item> adapter;
	ItemListClickHandler handler;

	String tag = this.getClass().getSimpleName();

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		Log.i(tag, "onActivityCreated");
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public void onAttach(Activity activity) {
		Log.i(tag, "onAttach");
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		Log.i(tag, "onCreate");
		super.onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState)
	{
		Log.i(tag, "onCreateView");
		view = inflater.inflate(R.layout.list_view_fragment,container, false);
		return view;		
	}

	public void updateAdapter()
	{
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onDestroy() {
		Log.i(tag, "onDestroy");
		super.onDestroy();
	}

	@Override
	public void onDestroyView() {
		Log.i(tag, "onDestroyView");
		super.onDestroyView();
	}

	@Override
	public void onDetach() {
		Log.i(tag, "onDetach");
		super.onDetach();
	}

	@Override
	public void onPause() {
		Log.i(tag, "onPause");
		super.onPause();
	}

	@Override
	public void onResume() {
		Log.i(tag, "onResume");
		super.onResume();
		
		Bundle category = this.getArguments();
		int position = category.getInt("position");

		ArrayList<Item> itemList = Utilities.getItemList();
		
		context = getActivity();	
		listView = (ListView) view.findViewById(R.id.listViewFragment);
		adapter = new ArrayAdapter<Item>(context, R.layout.text_row, R.id.textRow, itemList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
		
		try
		{
			handler = (ItemListClickHandler) getActivity();
		}
		catch (ClassCastException e)
		{
			Log.i(tag, "Activity " + getActivity().getClass().getSimpleName() + " FAILED!");
		}
	}

	@Override
	public void onStart() {
		Log.i(tag, "onStart");
		super.onStart();
	}

	@Override
	public void onStop() {
		Log.i(tag, "onStop");
		super.onStop();
	}

	/*
	 *  public interface ItmeListClickHandler: used to send the users click position of the list so that the system can
	 *  	make the appropriate updates 
	 * 
	 * */

	public interface ItemListClickHandler
	{
		public void onHandleItemClick(int position);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position , long arg3) 
	{	
		handler.onHandleItemClick(position);  
	}	
}
