package com.team.xmarketsstore;

public class Promotion {

	private String name;
	private String description;
	private String startDate;
	private String endDate;	
	private int promotionId;
	private int categoryId;
	private int storeId;

	public Promotion (String name, String description, String startDate, String endDate, int promotionId,
			int categoryId, int storeId) 
	{
		this.name = name;
		this.description = description;
		this.startDate = startDate;
		this.endDate = endDate;
		this.promotionId = promotionId;
		this.categoryId = categoryId;
		this.storeId = storeId;
	}

	/* GETTER AND SETTER METHODS*/
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public int getPromotionId() {
		return promotionId;
	}

	public void setPromotionId(int promotionId) {
		this.promotionId = promotionId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	
	
	
	
	
	
	
	
	
/*
 * THE CODE BELOW DOES NOT SEEM TO FIT IN THIS CLASS BUT MAY BE USEFUL ELSEWARE
 **/
	
//	public boolean DeletePromotion(Promotion d)
//	{
//		return collection_promotions.remove(d);
//	}
//
//	public boolean UpdatePromotion(Promotion a, String name, int id, String desc, String pic,String startdate, String enddate, int c_id, int s_id)
//	{
//		if(collection_promotions.contains(a)==true)
//		{
//			int index = collection_promotions.indexOf(a);
//			a.Name = name;
//			a.Id = id;
//			a.Description = desc;
//			a.Picture = pic;
//			a.StartDate = startdate;
//			a.EndDate=enddate;
//			a.Category_Id= c_id;
//			a.Store_Id= s_id;
//			collection_promotions.add(index, a);
//			return true;
//		}
//		return false;
//	}
//
//	public Promotion GetPromotionById(int id){
//		for(Promotion t : collection_promotions) {
//			if(t.Id == id) { 
//				return t;	   
//			}
//		}
//		return null;	
//	}
//	public ArrayList<Promotion> GetPromotionsByName(String name){
//		ArrayList<Promotion> output= new ArrayList<Promotion>();
//
//		for(Promotion c : collection_promotions) {
//			if(c.Name.equalsIgnoreCase(name)) {
//				output.add(c);	   
//			}
//		}
//		return output;
//	}
//	public ArrayList<Promotion> GetPromotionsByItem(Item d)
//	{
//		ArrayList<Promotion> output= new ArrayList<Promotion>();
//
//		for(Promotion c : collection_promotions) {
//			if(c.Id == d.getId()) {
//				output.add(c);	   
//			}
//		}
//		return output;
//	}
//
//	public ArrayList<Promotion> GetPromotionsByCategory(Category d){
//		ArrayList<Promotion> output= new ArrayList<Promotion>();
//
//		for(Promotion c : collection_promotions) {
//			if(c.Category_Id == d.getId()) {
//				output.add(c);	   
//			}
//		}
//		return output;
//	}
//
//	public ArrayList<Promotion> GetPromotionsByStore(int storeId){
//		ArrayList<Promotion> output= new ArrayList<Promotion>();
//
//		for(Promotion c : collection_promotions) {
//			if(c.Store_Id == storeId) {
//				output.add(c);   
//			}
//		}
//		return output;
//	}
}


