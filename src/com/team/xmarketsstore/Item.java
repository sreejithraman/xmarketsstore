package com.team.xmarketsstore;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class Item implements Parcelable {

	private String name;
	private String description;
	private String aisle; 
	private double price;
	private int xCoordinate;
	private int yCoordinate;
	private int quantity;
	private int itemId;
	private int categoryId;
	private int storeId;

	ArrayList<Integer> promotionIds = new ArrayList<Integer>();

	public Item (String name, String description, String aisle, int xCoordinate, int yCoordinate,
			double price, int quantity, int itemId, int categoryId, int storeId) 
	{
		this.name = name;
		this.description = description;
		this.aisle = aisle;
		this.price = price;
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
		this.quantity = quantity;
		this.itemId = itemId;
		this.categoryId = categoryId;
		this.storeId = storeId;
	}


	@Override
	public String toString() {
		return name;
	}

	/* GETTER AND SETTER METHODS */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAisle() {
		return aisle;
	}

	public void setAisle(String aisle) {
		this.aisle = aisle;
	}

	public int getXCoordinate() {
		return xCoordinate;
	}
	
	public int getYCoordinate() {
		return yCoordinate;
	}

	public void setXCoordinate(int coordinate) {
		this.xCoordinate = xCoordinate;
	}
	
	public void setYCoordinate(int coordinate) {
		this.yCoordinate = yCoordinate;
	}

	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getStoreId() {
		return storeId;
	}

	public void setStoreId(int storeId) {
		this.storeId = storeId;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public ArrayList<Integer> getPromotionIds() {
		return promotionIds;
	}

	public void setPromotionIds(ArrayList<Integer> promotionIds) {
		this.promotionIds = promotionIds;
	}


	//Method used for parsing the Items
	public Item(Parcel source) 
	{
		name = source.readString();
		description = source.readString();
		aisle = source.readString();
		xCoordinate = source.readInt();
		yCoordinate = source.readInt();
		quantity = source.readInt();
		itemId = source.readInt();
		categoryId = source.readInt();
		storeId = source.readInt();

		//Convert int[] to ArrayList<Integer>
		int[] temp = source.createIntArray();
		ArrayList<Integer> pId = new ArrayList<Integer>();
		for(int i = 0;i < temp.length;i++)
		{
			pId.add(temp[i]);
		}
		price = source.readDouble();
	}

	public static final Parcelable.Creator<Item> CREATOR = new Parcelable.Creator<Item>() {
		public Item createFromParcel(Parcel source) {
			return new Item(source);
		}
		public Item[] newArray(int size) {
			return new Item[size];
		}
	};
	
	@Override
	public void writeToParcel(Parcel dest, int flags) 
	{
		int[] pId = new int[promotionIds.size()];
		for(int i = 0;i < pId.length;i++)
		{
			pId[i] = promotionIds.get(i);
		}

		dest.writeString(name);
		dest.writeString(description);
		dest.writeString(aisle);
		dest.writeDouble(price);
		dest.writeInt(quantity);
		dest.writeInt(xCoordinate);
		dest.writeInt(yCoordinate);
		dest.writeInt(itemId);
		dest.writeInt(categoryId);
		dest.writeInt(storeId);
		dest.writeIntArray(pId);
	}
	
	@Override
	public int describeContents() {
		return 0;
	}
}

