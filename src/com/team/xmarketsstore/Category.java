package com.team.xmarketsstore;

import java.util.ArrayList;

import com.team.xmarketsstore.Item;

public class Category {
	
	private String name;
	private int categoryId;
	private int storeId;
	
	ArrayList<Item> categoryItems = new ArrayList<Item>();
	ArrayList<Integer> promotionIds = new ArrayList<Integer>();
	
	public Category (String name, int categoryId, int storeId, ArrayList<Item> categoryItems,
			ArrayList<Integer> promotionIds)
	{
		this.name = name;
		this.categoryId = categoryId;
		this.storeId = storeId;
		this.categoryItems = categoryItems;
		this.promotionIds = promotionIds;
	}

	
	@Override
	public String toString() {
		return name;
	}


	/* GETTERS AND SETTERS */
	public String getName() {
		return name;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public int getStoreId() {
		return storeId;
	}

	public ArrayList<Item> getCategoryItems() {
		return categoryItems;
	}

	public void setCategoryItems(ArrayList<Item> categoryItems) {
		this.categoryItems = categoryItems;
	}
	
	public ArrayList<Integer> promotionIds() {
		return promotionIds;
	}

	public void setPromotionIds(ArrayList<Integer> promotionIds) {
		this.promotionIds = promotionIds;
	}
	
	
	
	
	
	
	
	
/*
 * THE CODE BELOW DOES NOT SEEM TO FIT IN THIS CLASS BUT MAY BE USEFUL ELSEWARE
 **/	
	
//	public boolean DeleteCategory(Category d)
//	{
//		return collection_categories.remove(d);
//	}
//	
//	public boolean UpdateCategory(Category a, int id, String name, Promotion p, int storeId){
//		if(collection_categories.contains(a)==true)
//		{
//			int index = collection_categories.indexOf(a);
//			a.Id = id;
//			a.Name = name;
//			a.Promo = p;
//			a.Store_Id=storeId;
//			collection_categories.add(index, a);
//			return true;
//		}
//		return false;
//	}
//	
//	public Category GetCategoryById(int id){
//		 for(Category t : collection_categories) {
//			 if(t.Id == id) { 
//			      return t;	   
//			 }
//		 }
//		 return null;	
//	}
//	
//	public ArrayList<Category> GetCategoriesByName(String name){
//		ArrayList<Category> output= new ArrayList<Category>();
//
//		for(Category c : collection_categories) {
//			 if(c.Name.equalsIgnoreCase(name)) {
//			     output.add(c);	   
//			 }
//		}
//		 return output;
//	}
//	
//	public ArrayList<Category> GetCategoriesByItem(Item d)
//	{
//		ArrayList<Category> output= new ArrayList<Category>();
//
//		for(Category c : collection_categories) {
//			 if(c.Id == d.getCategoryId()) {
//			       output.add(c);	   
//			 }
//		 }
//		 return output;
//	}
//	
//	public ArrayList<Category> GetCategoriesByPromotion(Promotion p){
//		ArrayList<Category> output= new ArrayList<Category>();
//
//		for(Category c : collection_categories) {
//			 if(c.Promo == p) {
//			      output.add(c);	   
//			 }
//		 }
//		 return output;
//	}
//	
//	public ArrayList<Category> GetCategoriesByStore(Store d){
//		ArrayList<Category> output= new ArrayList<Category>();
//
//		for(Category c : collection_categories) {
//			 if(c.Store_Id == d.getId()) {
//			      output.add(c);   
//			 }
//		 }
//		 return output;
//	}
}
