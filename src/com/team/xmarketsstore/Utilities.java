package com.team.xmarketsstore;

import java.util.ArrayList;
import android.app.Application;

public class Utilities extends Application {

	private static ArrayList<Store> stores;
	private static ArrayList<Category> storeCategories;
	private static ArrayList<Promotion> storePromotions;
	private static ArrayList<Item> storeItems;
	
	public void onCreate()
	{
		super.onCreate();
		
		stores = new ArrayList<Store>();
		storeCategories = new ArrayList<Category>();
		storePromotions = new ArrayList<Promotion>();
		storeItems = new ArrayList<Item>();
		
		//addPresetStores();
		//addPresetStorePromotions();
		//addPresetStoreCategories();
		//addPresetStoreItems();
	}
	
	
	
	
	//-------------List Modifier Methods-------------------
	
	//-----Category------
	public static void addCategory(String name)
	{
		Category category = new Category(name, 0, 0, null, null);
		storeCategories.add(category);
	}
	
	public static ArrayList<Category> getCategoryList()
	{
//		String[] categories = new String[storeCategories.size()];
//		
//		for(int i=0; i<storeCategories.size(); i++)
//		{
//			categories[i] = storeCategories.get(i).toString();
//		}
		return storeCategories;
	}
	
	public static int getCategoryListSize()
	{
		return storeCategories.size();
	}
	
	
	//-----Item-----
	public static void addItem(String name)
	{
		Item item = new Item(name, null, null, 0, 0, 0, 0, 0, 0, 0);
		storeItems.add(item);
	}
	
	public static ArrayList<Item> getItemList()
	{
		return storeItems;
	}
	
	public static int getItemListSize()
	{
		return storeItems.size();
	}
	
	
	
	
	/* GETTER AND SETTER METHODS */
	public ArrayList<Store> getStores() {
		return stores;
	}

	public void setStores(ArrayList<Store> stores) {
		this.stores = stores;
	}

	public ArrayList<Category> getStoreCategories() {
		return storeCategories;
	}

	public void setStoreCategories(ArrayList<Category> storeCategories) {
		this.storeCategories = storeCategories;
	}

	public ArrayList<Promotion> getStorePromotions() {
		return storePromotions;
	}

	public void setStorePromotions(ArrayList<Promotion> storePromotions) {
		this.storePromotions = storePromotions;
	}

	public ArrayList<Item> getStoreItems() {
		return storeItems;
	}

	public void setStoreItems(ArrayList<Item> storeItems) {
		this.storeItems = storeItems;
	}
	
	

	/* ADD PRESETS TO LIST METHODS */
	public void addPresetStores() 
	{
		stores = new ArrayList<Store>();
		//Store marsh = new 
	}
	
	public void addPresetStorePromotions() 
	{
		storePromotions = new ArrayList<Promotion>();
	}
	
	public void addPresetStoreCategories() 
	{
		storeCategories = new ArrayList<Category>();
		Category meat = new Category("meat", 1, 2, null, null);
		Category vegies = new Category("vegies", 2, 2, null, null);
		Category dairy = new Category("dairy", 3, 2, null, null);
		Category bread = new Category("bread", 4, 2, null, null);
		Category cheese = new Category("cheese", 5, 2, null, null);
		Category tacos = new Category("tacos", 6, 2, null, null);
		Category srees = new Category("srees", 7, 2, null, null);
		Category cupcakes = new Category("srees", 8, 2, null, null);
		storeCategories.add(meat);
		storeCategories.add(vegies);
		storeCategories.add(dairy);
		storeCategories.add(bread);
		storeCategories.add(cheese);
		storeCategories.add(tacos);
		storeCategories.add(srees);
		storeCategories.add(cupcakes);
	}
	
	public void addPresetStoreItems() 
	{
		storeItems = new ArrayList<Item>();
		for(int i = 0; i < 10; i++)
		{
			Item item = new Item("Item "+i, "description "+i, "aisle "+1, 333, 333 , 3.33 + i, i, i, i, 333);
			storeItems.add(item);
		}
	}
}

