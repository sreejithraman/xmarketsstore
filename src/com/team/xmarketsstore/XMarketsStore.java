/*
 * @author Ashton Coulson
 * acoulso@purdue.edu
 * 
 *
 * When referencing the different fragments or Layouts in the code:
 *  Panels = the left or right side of the application as a whole. The panels 
 * 		contain all of the fragments and layouts on their side.
 *	Frames = the areas where the lists of Items, Promotions, ect.. are held 
 * 		and switched.
 * 
 * */

package com.team.xmarketsstore;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class XMarketsStore extends FragmentActivity 
implements CategoryListFragment.CategoryListClickHandler, ItemListFragment.ItemListClickHandler	{

	String tag = this.getClass().getSimpleName();

	View view;

	Fragment leftPanel = new LeftPanelFragment();
	Fragment rightPanel = new RightPanelFragment();
	Fragment itemList = new ItemListFragment();
	Fragment categoryList = new CategoryListFragment();
	Fragment promotionList = new PromotionListFragment();
	Fragment map = new MapFragment();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.xmarkets_store);
		
		//Loads fragments into the left and right application panels 
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.left_panel_layout, leftPanel, "LEFT");
		ft.replace(R.id.item_list_frame, categoryList, "CTGRY");
		ft.replace(R.id.right_panel_layout, rightPanel, "RIGHT");
		ft.replace(R.id.toggle_map_promo_frame, promotionList, "PROMO");
		
		ft.addToBackStack(null);
		ft.commit();
	}


	/*
	 * toggleMapPromo(View): Toggles the map and promotion screens within the right application panel
	 * 						  when the Map/Promotion button is clicked.
	 * */
	public void toggleMapPromo(View v) 
	{
		Log.i(tag, "Toggle Map/Promo Buton");
		
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		
		if (promotionList.isVisible()) 
		{
			TextView title = new TextView(this);
			Button swap = new Button(this);
			Button hide1 = new Button(this);
			Button hide2 = new Button(this);
			Button hide3 = new Button(this);

			title = (TextView)findViewById(R.id.mapPromoTitle);
			swap = (Button)findViewById(R.id.toggleMapPromoButton);
			hide1 = (Button)findViewById(R.id.addPromotionButton);
			hide2 = (Button)findViewById(R.id.applyPromotionButton);
			hide3 = (Button)findViewById(R.id.deletePromotionButton);

			title.setText("Map:");
			swap.setText("Promotions");
			hide1.setVisibility(v.INVISIBLE);
			hide2.setVisibility(v.INVISIBLE);
			hide3.setVisibility(v.INVISIBLE);

			ft.replace(R.id.toggle_map_promo_frame, map, "MAP");
		}
		else 
		{
			TextView title = new TextView(this);
			Button swap = new Button(this);
			Button show1 = new Button(this);
			Button show2 = new Button(this);
			Button show3 = new Button(this);

			title = (TextView)findViewById(R.id.mapPromoTitle);
			swap = (Button)findViewById(R.id.toggleMapPromoButton);
			show1 = (Button)findViewById(R.id.addPromotionButton);
			show2 = (Button)findViewById(R.id.applyPromotionButton);
			show3 = (Button)findViewById(R.id.deletePromotionButton);

			title.setText("Promotions:");
			swap.setText("Map");
			show1.setVisibility(v.VISIBLE);
			show2.setVisibility(v.VISIBLE);
			show3.setVisibility(v.VISIBLE);
			
			ft.replace(R.id.toggle_map_promo_frame, promotionList, "PROMO");
		}	

		ft.addToBackStack(null);
		ft.commit();
	}
	
	
	/*
	 *  addBySection(View v): Adds a new Category or Item Depending on which screen the user is at. 
	 *  	If the user is in the Item Details section, this button will act as an 'accept' button to  
	 * 		verify the current set item details.
	 * */
	public void addBySection(View v)
	{	
		Log.i(tag, "Add Section Button");
		
		if(categoryList.isVisible())
		{	
			Utilities.addCategory("new");
			((CategoryListFragment) categoryList).updateAdapter();
		}
		else if(itemList.isVisible())
		{
			Utilities.addItem("item!");
			((ItemListFragment) itemList).updateAdapter();
		}
	}
	
	
	/*
	 *  backUpSection(View v): Returns back to the Categories Fragment if the users is in Items. Returns back to the Items
	 *   	Fragment if the user is in the Item Details.
	 * */
	public void backUpSection(View v)
	{
		Log.i(tag, "Back Button!");
		
		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();

		if (itemList.isVisible()) 
		{
			TextView title = new TextView(this);
			title = (TextView)findViewById(R.id.sectionTitle);
			title.setText("Categories:");
			
			ft.replace(R.id.item_list_frame, categoryList, "CTGRY");
		}
		ft.addToBackStack(null);
		ft.commit();
	}


	/*
	 * Implemented Methods: onHandleItemClick(int position), onHandleItemClick(int position),
	 * 	used to pass the users position clicked within the item menus.
	 * 
	 * */
	@Override
	public void onHandleCategoryClick(int position) 
	{
		Log.i(tag, "clicked at position " + position);

		FragmentManager fm = getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		
		try
		{
			Bundle category = new Bundle();
			category.putInt("position", position);
			itemList.setArguments(category);
		}
		catch(Exception e)
		{
			Log.i(tag, "Exception in Handler"); //Fragment already active
		}
		
		TextView title = new TextView(this);
		title = (TextView)findViewById(R.id.sectionTitle);
		title.setText("Items:");
		
		Button showBackButton = new Button(this);
		showBackButton = (Button)findViewById(R.id.backCategoryButton);
		showBackButton.setVisibility(view.VISIBLE);

		ft.replace(R.id.item_list_frame, itemList, "ITEM");
		ft.addToBackStack(null);
		ft.commit();
	}


	@Override
	public void onHandleItemClick(int position) 
	{
		Log.i(tag, "clicked at position " + position);	
	}
}